package com.orsyp.help;

public class OnlineHelp {
	public static void displayHelp(int RC, String Message, String Release) {
		
		System.out.println(Message+"\n");
		System.out.println("------------------------------------------------------------------");
		System.out.println("++ This Tool Changes the Targets from Job definitions in Packages");
		System.out.println("    ++ All Targets will be set to \"use default node target\"    ");		
		System.out.println("------------------------------------------------------------------\n");
		System.out.println("++ Mandatory Parameters:\n");
		System.out.println("    -W: Password to UVMS");
		System.out.println("");
		System.out.println("++ Optional Parameters:\n");
		System.out.println("    -i: Package Name from UVMS [if not specified, all packages are taken into account]");
		System.out.println("    -l: Login for UVMS connection [if not specified, value taken from properties file]");
		System.out.println("    -u: UVMS host name [if not specified, value taken from properties file]");
		System.out.println("    -p: UVMS Connection port [if not specified, value taken from properties file]");
		System.out.println("    -v: Version Information & UVMS compatibility");
		System.out.println("");
		System.out.println("++ Display Help:\n");
		System.out.println("    -h: Display online help");
		System.out.println("");
		System.out.println("  IMPORTANT NOTE:");
		System.out.println("  => All Configuration parameters can be modified from properties file in same folder as Jar file");
		System.out.println("");
		System.out.println("++ Examples:\n");
		System.out.println("    pckMgt -W universe");
		System.out.println("    pckMgt -W universe -i MyPackage");
		System.exit(RC);
	}
	public static void displayVersion(int RC, String release, String[] APIversions) {
		System.out.println("Version "+release);
		System.out.println("++ This Tool was compiled and tested with the following UVMS:");
		for (int j=0;j < APIversions.length;j++){
			System.out.println(APIversions[j]);
		}
		System.out.println("  => Other versions of UVMS might not work as expected or might not work at all");
		System.exit(RC);
	}
}
