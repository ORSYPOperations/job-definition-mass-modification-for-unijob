package com.orsyp;

import java.io.IOException;
import java.util.Properties;

import com.orsyp.unijob.UVMSInterface;
import com.orsyp.unijob.UniPackageTest;
import com.orsyp.util.PropertyLoader;
import com.orsyp.help.*;

public class Go {
	static String Customer = "CLSA";
	static String Release="0.0.1";
	static String[] APIversions = {"300"};
	
	public static void main(String[] argv) throws IOException, InterruptedException {
			try {
				System.out.println("=======================================================");
				System.out.println("  **  ORSYP Package Management Tool version "+Release+"  **  ");
				System.out.println("  *          ORSYP Professional Services.           *  ");
				System.out.println("  *  Copyright (c) 2011 ORSYP.  All rights reserved.*  ");
				System.out.println("  *              Implemented for: "+Customer+"              *");
				System.out.println("=======================================================");
				System.out.println("");
				// Load Properties for the project
				String password="";
				String packageName="";
				
				boolean allPackages=false;
				String login="";
				String UVMSHost="";
				String port="";
				
				 GetOpt param = new GetOpt(argv, "hW:l:u:p:i:hv");
				 param.optErr = true;
			        int ch = -1;
			        while ((ch = param.getopt()) != GetOpt.optEOF) {
			        	// Online help
			            if ((char)ch == 'h'){ OnlineHelp.displayHelp(0,"Display help",Release);}
			            if ((char)ch == 'W'){password=param.optArgGet();}
			            if ((char)ch == 'i'){packageName=param.optArgGet();}
			            if ((char)ch == 'l'){login=param.optArgGet();}
			            if ((char)ch == 'u'){UVMSHost=param.optArgGet();}
			            if ((char)ch == 'p'){port=param.optArgGet();}
			            if ((char)ch == 'h'){OnlineHelp.displayHelp(0, "Display help", Release);}
			            if ((char)ch == 'v'){OnlineHelp.displayVersion(0,Release,APIversions);}
			        }
			        
			        Properties pGlobalVariables = PropertyLoader.loadProperties("PackageTargetMgt");
			        
				    if (password.equals("")){password=pGlobalVariables.getProperty("UVMS_PWD");}    
			        if (password.equals("")){OnlineHelp.displayHelp(-1,"Error: No Password Passed!",Release);}
			        if (packageName.equals("")){
			        	System.out.println(" !!! No Package Specified, All Packages will be processed.");
			        	allPackages=true;
			        	packageName="XXX_ALL_PACKAGES";
			        	}

				System.out.println("=> Load Properties for the project");
				
								
				// Connect to UVMS
				
	            if (login.equals("")){login=pGlobalVariables.getProperty("UVMS_USER");}
	            if (UVMSHost.equals("")){UVMSHost=pGlobalVariables.getProperty("UVMS_HOST");}
	            if (port.equals("")){port=pGlobalVariables.getProperty("UVMS_PORT");}
			
				UVMSInterface uvInterface = new UVMSInterface(
						UVMSHost,
						Integer.parseInt(port),
						login,
						password,
						pGlobalVariables.getProperty("DU_NODE"),
						pGlobalVariables.getProperty("DU_COMPANY"),
						Area.Exploitation);
				uvInterface.ConnectEnvironment();
				
				UniPackageTest uniPack = new UniPackageTest(uvInterface.getContext());
				uniPack.ListContent(packageName);
				
				System.out.println(" -> End of Package Processing.");

			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
	}
	
}
